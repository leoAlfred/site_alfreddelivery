$(document).ready(function () {
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        autoplay: 5000,
        loop: true
    });
});

// EVENTO CLICK COM ROLAGEM SUAVE
$(".navbar-alfred .navbar-nav>li>a").click(function (event) {
    event.preventDefault();
    if ($(document).width() < 768) {
        $('html,body').animate({ scrollTop: $(this.hash).offset().top - 50 }, 700);
        setTimeout(function () { $('.navbar-alfred .navbar-nav>li>a').blur(); }, 700);
        $('.navbar-toggle').click();
    } else {
        $('html,body').animate({ scrollTop: $(this.hash).offset().top - 80 }, 700);
        setTimeout(function () { $('.navbar-alfred .navbar-nav>li>a').blur(); }, 700);
    }
});

// AJAX RELOAD DO FORM DE CONTATO
function gravar() {
    var dados = $('#formulario').serialize();

    $.ajax({
        type: 'GET',
        url: '/Home/EmailContatoShopper',
        data: dados,
        success: function (data) {
            $('.success').css('display', 'block');
            $('.error').css('display', 'none');
            $('#formulario input').val("");
            $('#formulario textarea').val("");
            $('#formulario select').val("");
        }, error: function () {
            $('.error').css('display', 'block');
            $('.success').css('display', 'none');
        }
    });
    return false;
};